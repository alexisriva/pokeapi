# Generated by Django 4.0 on 2022-01-06 00:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('pokemon', '0005_evolution_remove_pokemon_evolution_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='evolution',
            name='evolves_to',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='+', to='pokemon.evolution'),
        ),
    ]
