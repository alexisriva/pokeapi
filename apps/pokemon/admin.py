from django.contrib import admin
from .models import Evolution, EvolutionChain, Pokemon, Type, PokemonType, Generation


class PokemonTypeInLine(admin.TabularInline):
    model = PokemonType
    fields = ['type', 'primary']
    can_delete = False
    extra = 0
    max_num = 2

    def has_change_permission(self, request, obj):
        return False


class PokemonAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'generation',
    )

    list_filter = (
        'generation',
    )

    search_fields = (
        'name',
    )

    inlines = [
        PokemonTypeInLine,
    ]


admin.site.register(Pokemon, PokemonAdmin)
admin.site.register(Type)
admin.site.register(Evolution)
admin.site.register(EvolutionChain)
admin.site.register(PokemonType)
admin.site.register(Generation)
