from django.urls import path, include

from rest_framework.routers import DefaultRouter

from .viewsets import PokemonViewSet, TypeViewSet


router = DefaultRouter()
router.register(r'pokemon', PokemonViewSet)
router.register(r'types', TypeViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
