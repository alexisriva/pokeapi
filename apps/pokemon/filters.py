from django_filters.rest_framework import FilterSet, CharFilter, NumberFilter

from .models import Pokemon


class PokemonFilter(FilterSet):
    gen = NumberFilter('generation')
    name = CharFilter('name', lookup_expr='icontains')
    type = CharFilter('types__type__name', lookup_expr='icontains')

    class Meta:
        model = Pokemon
        fields = ('name', 'gen', 'type')
