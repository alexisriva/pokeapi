from django.db.models.base import Model
from django.db.models.fields.related import ManyToManyField
from rest_framework.serializers import ModelSerializer, SerializerMethodField

from .models import Evolution, EvolutionChain, Pokemon, PokemonType, Type


class TypeSerializer(ModelSerializer):
    class Meta:
        model = Type
        fields = ('name', 'color',)


class PokemonTypeSerializer(ModelSerializer):
    type = TypeSerializer()

    class Meta:
        model = PokemonType
        fields = ('type', 'primary')


class NewPokemonTypeSerializer(ModelSerializer):
    class Meta:
        model = PokemonType
        fields = ('type', 'primary')


class SimplePokemonSerializer(ModelSerializer):
    class Meta:
        model = Pokemon
        fields = ('id', 'name', 'sprite',)


class TypedPokemonSerializer(ModelSerializer):
    types = PokemonTypeSerializer(many=True)

    class Meta:
        model = Pokemon
        fields = ('id', 'name', 'sprite', 'types',)


class EvolutionSerializer(ModelSerializer):
    pokemon = SimplePokemonSerializer()
    evolves_to = SerializerMethodField()

    class Meta:
        model = Evolution
        fields = ('pokemon', 'level', 'evolves_to',)

    def get_evolves_to(self, obj):
        if (obj.evolves_to):
            return EvolutionSerializer(obj.evolves_to).data


class EvolutionChainSerializer(ModelSerializer):
    chain = EvolutionSerializer()

    class Meta:
        model = EvolutionChain
        fields = ('chain',)


class PokemonSerializer(ModelSerializer):
    types = PokemonTypeSerializer(many=True)
    evolution_chain = EvolutionChainSerializer()

    class Meta:
        model = Pokemon
        fields = ('id', 'name', 'pokedex_entry', 'generation',
                  'sprite', 'evolution_chain', 'types',)


class NewPokemonSerializer(ModelSerializer):
    class Meta:
        model = Pokemon
        fields = ('id', 'name', 'pokedex_entry', 'generation', 'sprite',)
