from django.db import models
from django.utils.translation import gettext_lazy as _
from django.core.validators import MinValueValidator, MaxValueValidator


class Type(models.Model):
    name = models.CharField(_('name'), max_length=100)
    color = models.CharField(_('color'), max_length=50)

    class Meta:
        verbose_name = _('type')
        verbose_name_plural = _('types')

    def __str__(self):
        return self.name


class Generation(models.Model):
    id = models.SmallIntegerField(_('id'), primary_key=True)
    region = models.CharField(_('Region'), max_length=50)
    name = models.CharField(_('Name'), max_length=100)

    class Meta:
        verbose_name = _('generation')
        verbose_name_plural = _('generations')

    def __str__(self):
        return 'Gen {}'.format(self.id)


class Pokemon(models.Model):
    id = models.SmallIntegerField(_('id'), primary_key=True)
    name = models.CharField(_('name'), max_length=100)
    pokedex_entry = models.TextField(_('pokedex entry'))
    generation = models.ForeignKey(Generation, on_delete=models.PROTECT)
    sprite = models.CharField(_('sprite'), max_length=256)
    evolution_chain = models.ForeignKey(
        'EvolutionChain', null=True, blank=True, on_delete=models.PROTECT)

    class Meta:
        verbose_name = _('pokemon')
        verbose_name_plural = _('pokemon')

    def __str__(self):
        return self.name


class PokemonType(models.Model):
    pokemon = models.ForeignKey(
        Pokemon, on_delete=models.PROTECT, related_name='types')
    type = models.ForeignKey(
        Type, on_delete=models.PROTECT, related_name='pokemons')
    primary = models.BooleanField()

    class Meta:
        verbose_name = _('pokemon type')
        verbose_name_plural = _('pokemon types')

    def __str__(self):
        return '{} is a {} type'.format(self.pokemon.name, self.type.name)


class Evolution(models.Model):
    pokemon = models.ForeignKey(
        Pokemon, on_delete=models.PROTECT, related_name='+')
    level = models.SmallIntegerField(
        validators=[MinValueValidator(-1), MaxValueValidator(100)], default=-1)
    evolves_to = models.ForeignKey(
        'self', null=True, blank=True, on_delete=models.PROTECT, related_name='+')

    class Meta:
        verbose_name = _('evolution')
        verbose_name_plural = _('evolutions')

    def __str__(self):
        return '{} evolves to...'.format(self.pokemon.name)


class EvolutionChain(models.Model):
    chain = models.ForeignKey(
        Evolution, on_delete=models.CASCADE, related_name='chain')

    class Meta:
        verbose_name = _('evolution chain')
        verbose_name_plural = _('evolution chains')

    def __str__(self):
        return '{}\'s evolution family'.format(self.chain.pokemon.name)
