from django.shortcuts import get_object_or_404

from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet, ModelViewSet

from apps.pokemon.pagination import DefaultPokemonPagination
from .filters import PokemonFilter
from .models import Pokemon, Type
from .serializers import (PokemonSerializer, TypedPokemonSerializer,
                          TypeSerializer, NewPokemonSerializer, NewPokemonTypeSerializer)


class PokemonViewSet(ModelViewSet):
    queryset = Pokemon.objects.all()
    serializer_class = TypedPokemonSerializer
    serializer_action_classes = {
        'retrieve': PokemonSerializer,
    }
    filterset_class = PokemonFilter
    pagination_class = DefaultPokemonPagination

    def get_serializer_class(self):
        try:
            return self.serializer_action_classes[self.action]
        except:
            return super().get_serializer_class()

    def create(self, request):
        type_data_list = request.data.pop('type_data', [])
        if (len(type_data_list) == 0 or len(type_data_list) > 2):
            return Response(status=status.HTTP_400_BAD_REQUEST)

        new_pokemon = NewPokemonSerializer(data=request.data)
        if new_pokemon.is_valid():
            pokemon = new_pokemon.save()

            for type_data in type_data_list:
                poke_type = NewPokemonTypeSerializer(data=type_data)

                if (poke_type.is_valid()):
                    poke_type.save(pokemon=pokemon)
                else:
                    pokemon.delete()
                    return Response(status=status.HTTP_400_BAD_REQUEST)

            return Response(status=status.HTTP_201_CREATED)

        return Response(status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, _, pk=None):
        pokemon = get_object_or_404(Pokemon, pk=pk)
        pokemon.types.all().delete()
        pokemon.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class TypeViewSet(ReadOnlyModelViewSet):
    queryset = Type.objects.all()
    serializer_class = TypeSerializer
